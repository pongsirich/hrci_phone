import Vue from 'vue'
import VueRouter from 'vue-router'
import ErrorPage from '../views/error/ErrorPage.vue'
import Accessor from '../views/accessor/Accessor.vue'
import KPI from '../views/kpi/KpiOnline.vue'
import Kpisuccess from '../components/kpi/KpiSuccess.vue'
import KPIUploadFile from '../views/kpi/KpiUploadFile.vue'
import KPIBoardForm from '../views/kpi/KpiBoard.vue'
import Loading from '../views/Loading.vue'
import Home from '../views/covid19/Home.vue'
import CovidHistory from '../views/covid19/CovidHistory.vue'
import Company from '../views/covid19/Company.vue'
import Covid19 from '../views/covid19/Covid19.vue'
import TotalDashboard from '../views/covid19/TotalDashboard.vue'
import TotalDashboardDetail from '../views/covid19/TotalDashboardDetail.vue'
import Dashboard from '../views/covid19/Dashboard.vue'
import DetailDashboard from '../views/covid19/DetailDashboard.vue'
import HeadDashboard from '../views/covid19/DashboardGM.vue'
import DetailHeadDashboard from '../views/covid19/DetailHeadDashboard.vue'
// import KpiHome from '../views/kpi/KpiHome.vue'
// import Committee from '../views/kpi/KpiCommittee.vue'
import KpiEvaluate from '../views/kpi/KpiHomeSecurity.vue'
import KpiApprove from '../views/kpi/KpiHomeSecurityApprove.vue'
import KpiCommittee from '../views/kpi/KpiCommitteeSecurity.vue'
import Excel from '../views/Excel.vue'
// import FinalGrade from '../views/kpi/FinalGrade.vue'


Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'covid19',
    component: Covid19,
    children:[
      {
        path: 'totaldashboard/:oneid',
        name: 'dashboard',
        component: TotalDashboard
      },
      {
        path: 'totaldashboarddetail/:oneid',
        name: 'dashboard',
        component: TotalDashboardDetail
      },
      {
        path: 'headdashboard/:oneid',
        name: 'dashboard',
        component: HeadDashboard
      },
      {
        path: 'detailheaddashboard/:oneid',
        name: 'dashboard',
        component: DetailHeadDashboard
      },
      {
        path: 'detaildashboard/:oneid',
        name: 'dashboard',
        component: DetailDashboard
      },
      {
        path: 'dashboard/:oneid',
        name: 'dashboard',
        component: Dashboard
      },
      {
        path: 'home/:oneid',
        name: 'home',
        component: Home
      },
      {
        path: 'company/:oneid',
        name: 'home',
        component: Company
      },
      {
        path: 'back_history/:oneid',
        name: 'history_covid',
        component: CovidHistory
      }
    ]
  },
  {
    path: '/loading',
    name: 'loading',
    component: Loading
  },
  {
    path: '/accessor/:employeeId',
    name: 'accessor',
    component: Accessor
  },
  {
    path: '/kpionline/:employeeId',
    name: 'kpionline',
    component: KPI
  },
  {
    path: '/excel',
    name: 'kpioexcelnline',
    component: Excel
  },
  // {
  //   path: '/kpihome/:accountId',
  //   name: 'kpihome',
  //   component: KpiHome
  // },
  {
    path: '/kpi_evaluate/home/:token_onechat',
    name: 'kpi_evaluate',
    component: KpiEvaluate
  },
  {
    path: '/kpi_approve/:token_onechat',
    name: 'kpi_approve',
    component: KpiApprove
  },
  // {
  //   path: '/final_grade/:token_onechat',
  //   name: 'final_grade',
  //   component: FinalGrade
  // },
  // {
  //   path: '/kpicommittee/:accountId',
  //   name: 'kpicommittee',
  //   component: Committee
  // },
  {
    path: '/kpi_committee/:token_onechat',
    name: 'kpicommittee',
    component: KpiCommittee
  },
  {
    path: '/kpisuccess/:employeeId/:year/:term',
    name: 'kpisuccess',
    component: Kpisuccess
  },
  {
    path: '/kpiupload/:employeeId',
    name: 'kpiupload',
    component: KPIUploadFile
  },
  {
    path: '/kpiboard/:employeeId',
    name: 'kpiboard',
    component: KPIBoardForm
  },
  {
    path: '*',
    meta: { public: true },
    name: 'employee_not_found',
    component: ErrorPage
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
