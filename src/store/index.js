import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    sourceData: [],
    dialogEmpKpiMulti: false,
    employee: {},
    dataPageTogether: {},
    idTransfer: [],
    employeeTransfer: {},
    positionLists: [],
    sourceCompany:[],
    sourceDepartments:[],
    sourceEmployee:[],
    sourceStaffid:[],
    sourceGM:[],
    detailDashboard:[],
    HeadDepartment:[],
    dialogCancelEmpKpi: false,
    sourceCancelEmpKpi: {},
    dialogEmpKpiForm: false,
    dialogEmpKpiFormCommittee: false,
    dialogEmpKpiTransfer: false,
    portfolioLists: [
      {
        expectedPortfolio: '',
        ExpectedLevel: '',
        CanDoLevel: '',
        summaryLevel: '',
        weightPortfolio: '',
        totalPoint: '',
        commentLevel_B_Up: ''
      }
    ],
    isEditPortfolioLists: false,
    employeeInfo: {},
    employeeBoard: {},
    employeeId: '',
    viewMode: false,
    isLeader: false,
    dialogLoading: false,
    dialogLoadingType: 'success',
    dialogLoadingText: 'fish',
    snackbar: false
  },
  mutations: {
    updateSourceData: (state, payload) => {
      state.sourceData = payload
    },
    updateSourceDataJop: (state, payload) => {
      state.sourceDataJop = payload
    },
    updateDialogEmpKpiMulti: (state, payload) => {
      state.dialogEmpKpiMulti = payload
    },
    updateEmployeeTransfer: (state, payload) => {
      state.employeeTransfer = payload
    },
    updateEmployee: (state, payload) => {
      state.employee = payload
    },
    updateIdTransfer: (state, payload) => {
      state.idTransfer = payload
    },
    updateDataPageTogether: (state, payload) => {
      state.dataPageTogether = payload
    },
    updateSourceStaffid:(state, payload) => {
      state.sourceStaffid = payload;
    },
    updateSourceCompany:(state, payload) => {
      state.sourceCompany = payload;
    },
    updateSourceGM:(state, payload) => {
      state.sourceGM = payload;
    },
    updateDetailDashboard:(state, payload) => {
      state.detailDashboard = payload;
    },
    updateHeadDepartment:(state, payload) => {
      state.HeadDepartment = payload;
    },
    updateSourceDepartments:(state, payload) => {
      state.sourceDepartments = payload;
    },
    updateSourceEmployee:(state, payload) => {
      state.sourceEmployee = payload;
    },
    updateEmployeeId: (state, payload) => {
      state.employeeId = payload;
    },
    updateDialogLoading: (state, payload) => {
      state.dialogLoading = payload;
    },
    updatePositionLists: (state, payload) => {
      state.positionLists = payload;
    },
    updateDialogCancelEmpKpi: (state, payload) => {
      state.dialogCancelEmpKpi = payload;
    },
    updateDialogTransferEmpKpi: (state, payload) => {
      state.dialogEmpKpiTransfer = payload;
    },
    updateSourceCancelEmpKpi: (state, payload) => {
      state.sourceCancelEmpKpi = payload;
    },
    updateDialogEmpKpiForm: (state, payload) => {
      state.dialogEmpKpiForm = payload;
    },
    updateDialogEmpKpiFormCommittee: (state, payload) => {
      state.dialogEmpKpiFormCommittee = payload;
    },
    updatePortfolioLists: (state, payload) => {
      if (payload.length === 0) {
        state.portfolioLists = [
          {
            expectedPortfolio: '',
            ExpectedLevel: '',
            CanDoLevel: '',
            summaryLevel: '',
            weightPortfolio: '',
            totalPoint: '',
            commentLevel_B_Up: ''
          }
        ];
        state.isEditPortfolioLists = false;
      } else {
        state.portfolioLists = payload;
        state.isEditPortfolioLists = true;
      }
    },
    updateEmployeeInfo: (state, payload) => {
      state.employeeInfo = payload;
    },
    updateEmployeeBoard: (state, payload) => {
      state.employeeBoard = payload;
    },
    updateViewMode: (state, payload) => {
      state.viewMode = payload;
    },
    updateIsLeader: (state, payload) => {
      state.isLeader = payload;
    },
    updateSnackbar: (state, payload) => {
      state.snackbar = payload;
    }
  },
  actions: {},
  modules: {}
});
