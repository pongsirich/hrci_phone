import axios from 'axios';

export default {
  getEmployeeIdAccessor(employeeId) {
    return axios.get(
      `${process.env.VUE_APP_URL_2}/checkuuidAccessor/${employeeId}`
    );
  },
  getEmployeeIdBoard(employeeId) {
    return axios.get(
      `${process.env.VUE_APP_URL_2}/checkuuidBoard/${employeeId}`
    );
  },
  getAllEmployee() {
    return axios.post(`${process.env.VUE_APP_URL_2}/QryEmployeeMobile`);
  },
  getAllPositions() {
    return axios.post(`${process.env.VUE_APP_URL_2}/QryPosition`);
  },
  getEmployeeAccessor(employeeId, config) {
    return axios.get(
      `${process.env.VUE_APP_URL_2}/Qry_user_kpi_mobile/${employeeId}`,
      config
    );
  },
  getEmployeeKpi(employeeId, config) {
    return axios.get(
      `${process.env.VUE_APP_URL_2}/Qry_kpi_mobile/${employeeId}`,
      config
    );
  },
  getEmployeeKpiOne(employeeId, year, term) {
    return axios.get(
      `${process.env.VUE_APP_URL_2}/Qry_employee_kpi_one/${employeeId}/${year}/${term}`
    );
  },
  cancelEmpKpi(payload) {
    return axios.post(
      `${process.env.VUE_APP_URL_2}/cancel_emp_kpi_mobile`,
      payload
    );
  },
  transferKpi(payload) {
    return axios.post(
      `${process.env.VUE_APP_URL_2}/transfer_kpi_mobile`,
      payload
    );
  },
  confirmTransferKpiOne(payload) {
    return axios.post(
      `${process.env.VUE_APP_URL_2}/confirm_transfer_kpi_one`,
      payload
    );
  },
  confirmAllKpi(payload) {
    return axios.post(
      `${process.env.VUE_APP_URL_2}/confirm_all_kpi_mobile`,
      payload
    );
  },
  confirmAllEmpKpi(payload) {
    return axios.post(
      `${process.env.VUE_APP_URL_2}/confirm_all_emp_kpi_mobile`,
      payload
    );
  },
  confirmGradeKpiOne(payload) {
    return axios.post(
      `${process.env.VUE_APP_URL_2}/confirm_grade_kpi_one`,
      payload
    );

  },
  addProjectKpi(payload) {
    return axios.post(
      `${process.env.VUE_APP_URL_2}/add_project_mobile`,
      payload
    );
  },
  editProjectKpi(payload) {
    return axios.post(
      `${process.env.VUE_APP_URL_2}/edit_project_mobile`,
      payload
    );
  },
  getEmployeePresent(employeeId, config) {
    return axios.get(
      `${process.env.VUE_APP_URL_2}/Qry_user_present_mobile/${employeeId}`,
      config
    );
  },
  uploadFilePresent(payload, config) {
    return axios.post(
      `${process.env.VUE_APP_URL_2}/test_upload`,
      payload,
      config,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    );
  },
  downloadFilePresent(payload) {
    return `${process.env.VUE_APP_URL_2}/userGetPresent/${payload.year}/${payload.term}/${payload.present_file}`;
  },
  getEmployeeBoard(employeeId, config) {
    return axios.get(
      `${process.env.VUE_APP_URL_2}/Qry_board_present_mobile/${employeeId}`,
      config
    );
  },
  updateGradeBoard(payload) {
    return axios.post(`${process.env.VUE_APP_URL_2}/Update_board_kpi`, payload);
  },
  getEmployeeLists(payload) {
    return axios.post(`${process.env.VUE_APP_URL_2}/QryEmployee_kpi_one`, payload);
  }
};
